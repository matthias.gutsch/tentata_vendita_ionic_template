import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { RouterLink, RouterLinkActive } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CardModule } from 'primeng/card';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
  standalone: true,
  imports: [IonicModule, RouterLink, RouterLinkActive, CommonModule, CardModule],
})
export class AppComponent {
  public appPages = [
    { title: 'Homepage', url: '/', icon: 'mail' },
    { title: 'Lista', url: '/list', icon: 'paper-plane' },
    { title: 'Clienti', url: '/client-list', icon: 'paper-plane' },
    { title: 'Lista Checkbox', url: '/checkbox-list', icon: 'paper-plane' },
    { title: 'Utente', url: '/user', icon: 'paper-plane' },
    { title: 'Documenti', url: '/documents', icon: 'paper-plane' },
    { title: 'Configuration', url: '/configuration', icon: 'paper-plane' },
    { title: 'Operatore', url: '/operator-detail', icon: 'paper-plane' },

  ];
  public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];
  constructor() {}
}
