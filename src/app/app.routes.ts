import { Routes } from '@angular/router';

export const routes: Routes = [
  {
    path: '',
    loadComponent: () =>
      import('./folder/folder.page').then((m) => m.FolderPage),
  },


  {
    path: 'list',
    loadComponent: () =>
      import('./list/list.page').then((m) => m.ListPage),
  },

  {
    path: 'client-list',
    loadComponent: () =>
      import('./client-list/client-list.page').then((m) => m.ClientListPage),
  },
  {
    path: 'operator-detail',
    loadComponent: () =>
      import('./operator-detail/operator-detail.page').then((m) => m.OperatorDetailPage),
  },


  {
    path: 'configuration',
    loadComponent: () =>
      import('./configuration/configuration.page').then((m) => m.ConfigurationPage),
  },


  {
    path: 'user',
    loadComponent: () =>
      import('./user/user.page').then((m) => m.UserPage),
  },

  {
    path: 'documents',
    loadComponent: () =>
      import('./documents/documents.page').then((m) => m.DocumentsListPage),
  },

  {
    path: 'checkbox-list',
    loadComponent: () =>
      import('./checkbox-list/checkbox-list.page').then((m) => m.CheckboxListPage),
  },

];
