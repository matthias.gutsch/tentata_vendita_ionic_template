import { ComponentFixture, TestBed } from '@angular/core/testing';
import { provideRouter } from '@angular/router';
import { IonicModule } from '@ionic/angular';

import { CheckboxListPage } from './checkbox-list.page';

describe('ListPage', () => {
  let component: CheckboxListPage;
  let fixture: ComponentFixture<CheckboxListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CheckboxListPage, IonicModule],
      providers: [provideRouter([])],
    }).compileComponents();

    fixture = TestBed.createComponent(CheckboxListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
