import { ComponentFixture, TestBed } from '@angular/core/testing';
import { provideRouter } from '@angular/router';
import { IonicModule } from '@ionic/angular';

import { DocumentsListPage } from './documents.page';

describe('ListPage', () => {
  let component: DocumentsListPage;
  let fixture: ComponentFixture<DocumentsListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DocumentsListPage, IonicModule],
      providers: [provideRouter([])],
    }).compileComponents();

    fixture = TestBed.createComponent(DocumentsListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
