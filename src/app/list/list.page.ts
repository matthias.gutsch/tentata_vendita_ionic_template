import { CommonModule } from '@angular/common';
import { Component, inject, OnInit } from '@angular/core';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CardModule } from 'primeng/card';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  standalone: true,
  imports: [IonicModule, CardModule, RouterModule, CommonModule],
})
export class ListPage implements OnInit {
  public folder!: string;
  cards: any = [];
  private activatedRoute = inject(ActivatedRoute);
  constructor() {}

  ngOnInit() {
    this.folder = this.activatedRoute.snapshot.paramMap.get('id') as string;

    this.cards = [
        {
          id: 1,
        },
        {
          id: 2
        },
        {
          id: 3
        },
        {
          id: 4
        },
        {
          id: 1,
        },
        {
          id: 2
        },
        {
          id: 3
        },
        {
          id: 4
        },
        {
          id: 1,
        },
        {
          id: 2
        },
        {
          id: 3
        },
        {
          id: 4
        }
    ]
  }
}
