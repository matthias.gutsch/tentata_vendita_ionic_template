import { Component, inject, OnInit } from '@angular/core';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ButtonModule } from 'primeng/button';
import { CardModule } from 'primeng/card';

@Component({
  selector: 'app-operator-detail',
  templateUrl: './operator-detail.page.html',
  styleUrls: ['./operator-detail.page.scss'],
  standalone: true,
  imports: [IonicModule, CardModule, RouterModule,ButtonModule],
})
export class OperatorDetailPage implements OnInit {
  public folder!: string;
  private activatedRoute = inject(ActivatedRoute);
  constructor() {}

  ngOnInit() {
    this.folder = this.activatedRoute.snapshot.paramMap.get('id') as string;
  }
}
