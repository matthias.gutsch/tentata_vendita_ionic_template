import { ComponentFixture, TestBed } from '@angular/core/testing';
import { provideRouter } from '@angular/router';
import { IonicModule } from '@ionic/angular';

import { OperatorDetailPage } from './operator-detail.page';

describe('FolderPage', () => {
  let component: OperatorDetailPage;
  let fixture: ComponentFixture<OperatorDetailPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [OperatorDetailPage, IonicModule],
      providers: [provideRouter([])],
    }).compileComponents();

    fixture = TestBed.createComponent(OperatorDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
