import { CommonModule } from '@angular/common';
import { Component, inject, OnInit } from '@angular/core';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CardModule } from 'primeng/card';
import { InputTextModule } from 'primeng/inputtext';
import { TagModule } from 'primeng/tag';
@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.page.html',
  standalone: true,
  imports: [IonicModule, CardModule, RouterModule, TagModule, CommonModule, InputTextModule],
})
export class ClientListPage implements OnInit {
  public folder!: string;
  cards: any = [];
  private activatedRoute = inject(ActivatedRoute);
  constructor() {}

  ngOnInit() {
    this.folder = this.activatedRoute.snapshot.paramMap.get('id') as string;

    this.cards = [
        {
          id: 1,
        },
        {
          id: 2
        },
        {
          id: 3
        },
        {
          id: 4
        },
        {
          id: 1,
        },
        {
          id: 2
        },
        {
          id: 3
        },
        {
          id: 4
        },
        {
          id: 1,
        },
        {
          id: 2
        },
        {
          id: 3
        },
        {
          id: 4
        }
    ]
  }
}
